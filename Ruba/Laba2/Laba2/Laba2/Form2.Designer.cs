﻿namespace Laba2
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Корги");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Шарпей");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Пикинес");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Домашние", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3});
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Такса");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Бигль");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Бассет");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Охотничьи", new System.Windows.Forms.TreeNode[] {
            treeNode5,
            treeNode6,
            treeNode7});
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.BackColor = System.Drawing.Color.Yellow;
            this.treeView1.Location = new System.Drawing.Point(12, 43);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "Korgi";
            treeNode1.Text = "Корги";
            treeNode2.Name = "Sharpey";
            treeNode2.Text = "Шарпей";
            treeNode3.Name = "Piqinese";
            treeNode3.Text = "Пикинес";
            treeNode4.Name = "Home";
            treeNode4.Text = "Домашние";
            treeNode5.Name = "Taksa";
            treeNode5.Text = "Такса";
            treeNode6.Name = "Bigl\'";
            treeNode6.Text = "Бигль";
            treeNode7.Name = "Busset";
            treeNode7.Text = "Бассет";
            treeNode8.Name = "Hunt";
            treeNode8.Text = "Охотничьи";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode4,
            treeNode8});
            this.treeView1.Size = new System.Drawing.Size(121, 97);
            this.treeView1.TabIndex = 0;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.treeView1);
            this.Name = "Form2";
            this.Text = "Породы собак";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
    }
}