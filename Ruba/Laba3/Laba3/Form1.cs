﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laba3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
           if (MessageBox.Show("Сейчас переместим породы", "Сообщение", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                listBox2.Items.Add(listBox1.SelectedItem);
                listBox1.Items.Remove(listBox1.SelectedItem);
            } 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox2.SelectedItem != null)
            {
                listBox1.Items.Add(listBox2.SelectedItem);
                listBox2.Items.Remove(listBox2.SelectedItem);
            }
            else
            {


                MessageBox.Show("Нечего переносить", "Сообщение", MessageBoxButtons.OK, MessageBoxIcon.Error) ; }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int CurrentItem;
            CurrentItem = 0;
            int CountItems;
            CountItems = listBox1.Items.Count;
            while(CountItems> 0)
            { listBox2.Items.Add(listBox1.Items[CurrentItem]);
                CurrentItem = CurrentItem + 1;
                CountItems = CountItems - 1;
            }CurrentItem = 0;
            CountItems = listBox1.Items.Count;
            while (CountItems > 0)
            {
                listBox1.Items.Remove(listBox1.Items[CurrentItem]);
                CountItems = CountItems - 1;
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            foreach (object SelectedItem in listBox1.SelectedItems)
            {
                listBox2.Items.Add(SelectedItem);
            }
            while (listBox1.SelectedItem != null)
            {
                listBox1.Items.Remove(listBox1.SelectedItem);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            object NewItem;
            NewItem = listBox1.Items[0];


         
            listBox1.Items.Add(NewItem);

            int LastItem;
            LastItem = listBox1.Items.Count - 1;
            listBox1.Items[LastItem] = textBox1.Text;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Удалить породу?", "Предупреждение", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.OK)
            {
                listBox1.Items.Remove(listBox1.SelectedItem);
            }
        }
    }
}
