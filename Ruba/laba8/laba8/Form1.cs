﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace laba8
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int b = (int)numericUpDown2.Value;
            for (int i = 0; i < b; i++)
            {
                string colName = "Column" + i;
                string text = "Столбец" + i;
                dataGridView1.Columns.Add(colName, text);
            }
            int a = (int)numericUpDown1.Value;
            dataGridView1.Rows.Add(a);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Random r = new Random();
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    cell.Value = r.Next(0, 10);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)

        {
            if (dataGridView1.SelectedRows.Count != 0)
            {
                dataGridView1.Rows.Remove(dataGridView1.SelectedRows[0]);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    int v = (int)cell.Value;
                    if (v > 7)
                    {
                        Font f = new Font(dataGridView1.DefaultCellStyle.Font, FontStyle.Bold);
                        cell.Style.ForeColor = Color.Red;
                        cell.Style.SelectionForeColor = Color.Red;
                        cell.Style.BackColor = Color.LightGreen;
                        cell.Style.SelectionBackColor = Color.LightGreen;
                        cell.Style.Font = f;
                    }
                }
            }
        }
    }
}

