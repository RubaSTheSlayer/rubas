﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laba_5
{
    public partial class Form1 : Form
    {
        bool isDragging;
        int OldX;
        int OldY;
        Rectangle myRectangle2 = new Rectangle(200, 200, 200, 200);

        public Form1()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Drawing.Pen myPen;
            myPen = new System.Drawing.Pen(System.Drawing.Color.Tomato);
            Rectangle myRectangle = new Rectangle(100, 50, 80, 40);
            Graphics myGraphics = this.CreateGraphics();
            myGraphics.DrawEllipse(myPen, myRectangle);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            System.Drawing.Pen myPen;
            myPen = new System.Drawing.Pen(System.Drawing.Color.Blue);
            Rectangle myRectangle = new Rectangle(100, 100, 50, 50);
            Graphics myGraphics = this.CreateGraphics();
            myGraphics.DrawEllipse(myPen, myRectangle);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form2 ff = new Form2();
            ff.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form3 ffa = new Form3();
            ffa.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form4 ffd = new Form4();
            ffd.Show();
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            isDragging = true;
            OldX = e.X;
            OldY = e.Y;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if(isDragging == true)
            {
                pictureBox1.Top=pictureBox1.Top + (e.Y - OldY);
                pictureBox1.Left = pictureBox1.Left + (e.X - OldX);
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            isDragging = false;
            if (myRectangle2.Contains(pictureBox1.Bounds))
            {
                MessageBox.Show("Вы выйграли");
            }
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.FillRectangle(Brushes.AntiqueWhite, myRectangle2);
            g.DrawString("Поместить сюда!", new Font("Times New Roman", 25), Brushes.Red, myRectangle2);
        }
    }
}
