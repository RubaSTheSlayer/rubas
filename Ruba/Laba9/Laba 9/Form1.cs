﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Laba_9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {
                treeView1.Nodes.Add(textBox1.Text);
            }
            if (radioButton2.Checked)
            {
                if(treeView1.SelectedNode == null)
                {
                    MessageBox.Show("Не выбрана страна происхождения породы!");
                    return;
                }
                treeView1.SelectedNode.Nodes.Add(textBox1.Text);
            }
       
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                treeView1.ExpandAll();
            }
            else
            {
                treeView1.CollapseAll(); 
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            switch (treeView1.SelectedNode.Level)
            {
                case 0:
                    int A = treeView1.SelectedNode.Nodes.Count;
                    String text1 = treeView1.SelectedNode.Text;
                    String text2 = ", Количество пород:" + A.ToString() + "видов";
                    label1.Text = "Страна:" + text1 + text2;
                    string B = "-";
                    foreach(TreeNode item in treeView1.SelectedNode.Nodes)
                    {
                        B = B + item.Text + "-";
                    }
                    label2.Text = B;
                    break;
                case 1:
                    String text3 = treeView1.SelectedNode.Text;
                    string text4 = ", Страна происхождения" + treeView1.SelectedNode.Parent.Text;
                    label1.Text = "Это" + text3 + text4;
                    label1.Text = "";
                    break;
                default:
                    break;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DateTime date = dateTimePicker1.Value;
            date = date.AddDays(10);
            monthCalendar1.SetDate(date);
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            DateTime date = monthCalendar1.SelectionEnd;
            label3.Text = date.ToShortDateString();
            label4.Text = date.DayOfWeek.ToString();
            TimeSpan diff = date - dateTimePicker1.Value;
            label5.Text = diff.Days.ToString();
        }
    }
}
